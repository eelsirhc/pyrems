.. pyREMS documentation master file, created by
   sphinx-quickstart on Tue Oct  9 09:03:14 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pyREMS
==================================

Contents:

.. toctree::
   :maxdepth: 2
	
   installation
   usage
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

