Installation
==============
Python setup
-------------
If you haven't installed a Python library before, it's likely that your Python install is not configured to allow you (as a non-root user) to install the library. If you want to install as ``root`` you can skip this section and use ``sudo`` where necessary.

The easiest way to configure Python to enable custom libraries is to use the ``pydistutils`` file. Create a file called ``~/.pydistutils.cfg`` with the following lines: 

.. code-block:: python

  [install]
  install_lib = ~/LIBRARY_DIRECTORY/Python/2.7/site-packages
  install_scripts = ~/EXECUTABLE_DIRECTORY

replacing ``LIBRARY_DIRECTORY`` with a suitable location for your libraries (`lib` or `.python_library`, for example) and ``EXECUTABLE_DIRECTORY`` with the location of your binary/executable files (e.g. `bin`). You will also need to add the executable directory to your shell path (so that you can run the scripts you install) and create a shell variables called ``PYTHONPATH`` that points to the library directory. e.g. in bash

.. code-block:: bash

  export PYTHONPATH=~/LIBRARY_DIRECTORY/Python/2.7/site-packages
  PATH=$PATH:$HOME/EXECUTABLE_DIRECTORY

 and csh

.. code-block:: tcsh

  set PYTHONPATH=~/LIBRARY_DIRECTORY/Python/2.7/site-packages
  set PATH=$PATH:$HOME/EXECUTABLE_DIRECTORY

Finally, close the window or source the .cshrc (or .bashrc) file, and you should be able to custom Python libraries.

From the Repository
--------------------
The repository is stored on ``bitbucket``. Clone the repository as 

.. code-block:: bash

   git clone git@bitbucket.org:eelsirhc/pyrems.git

This will create a directory called ``pyrems``. Inside this directory run the following statement

.. code-block:: bash

  python setup.py install

This will install the pyrems package into your Python libraries, and the executable in your ``bin`` directory.
