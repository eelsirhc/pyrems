Usage
=======

The pyREMS package contains an executable called ``pyrems`` that should have been
installed to a suitable location in your path. Once this is done you can type
``pyrems help`` to see the options available

.. code-block:: bash
   
   usage: pyrems [-h]
                 
                 {printXML,writeClimate,writeJSON,process,
				  printJSON,archiveLog,downloadXML,printString,compareXML}
                 ...
   
   positional arguments:
     {printXML,writeClimate,writeJSON,process,
	  printJSON,archiveLog,downloadXML,printString,compareXML}
       downloadXML         Download...
       compareXML          Compare ...
       printXML            Print ...
       printJSON           Convert ...
       writeJSON           Convert ...
       printString         Pretty ...
       process             wrapper ...
       archiveLog          Archives ...
       writeClimate        Convert ...
   
   optional arguments:
     -h, --help            show this help message and exit


Most of these commands are for debugging purposes. The most useful command is ``process``, which will 
  * download the rems_weather.xml file
  * compare it against the archive
  * update the archive if necessary
  * regenerate the rems_climate.xml file
  * regenerate a history.rems_weather.json file
  * run a ``callback`` script if there is new data in the archive.

The easiest way to run this script for the rems data is without any arguments. so

.. code-block:: bash
	
	pyrems process
	
This will use the default values in the code (downloading from CAB, storing as rems_weather.xml.YYYYMMDD_HHmmss, using the directory name `archive`, filename rems_climate.xml, etc). You can change the filenames and 
logging behavior with command line options. To find these options type ``pyrems help process`` (or ``pyrems process -h``)

.. code-block:: bash

  usage: pyrems process [-h] [-u URLNAME] [-d DIRNAME] [-f FILENAME]
                         [--oldfilename OLDFILENAME] [-a ARCHIVE]
                         [--output OUTPUT] [--climate_output CLIMATE_OUTPUT]
                         [--compression COMPRESSION] [-n] [-s SUCCESS_CALLBACK]
   
  optional arguments:
     -h, --help            show this help message and exit
     -u URLNAME, --urlname URLNAME
     -d DIRNAME, --dirname DIRNAME
     -f FILENAME, --filename FILENAME
     --oldfilename OLDFILENAME
     -a ARCHIVE, --archive ARCHIVE
     --output OUTPUT
     --climate_output CLIMATE_OUTPUT
     --compression COMPRESSION
     -n, --nolog
     -s SUCCESS_CALLBACK, --success_callback SUCCESS_CALLBACK

Example Usage
---------------
I have a program configured to periodically run this command, and email me updates if there is new data. I use a `cron` job to run the code using the crontab line

.. code-block:: bash
   
   0 0-23/2 * * * archive_location/rems_cron

The ``rems_cron`` script is a bash script used to ensure that my Python environment is configured in non-interactive mode

.. code-block:: bash

   #!/bin/bash
   cd archive_location
   export PYTHONPATH=$HOME/Library/Python/2.7/site-packages:$PYTHONPATH
   ~/bin/pyrems process --success_callback="./email_data"
   
This script will run the pyrems code and if new data is found, will also run the ``./email_data`` script. The ``email_data`` script then contains

.. code-block:: bash

   #!/bin/bash
   tidy --quiet yes --wrap 0 -xml -m -i rems_climate.xml
   ./email_me -s Rems-full -f rems_climate.xml
   ./email_me -s Rems-Data -f rems_weather.xml

The ``tidy`` command indents the rems_climate.xml file, then the ``email_me`` command sends both the full climate file and the latest weather file in separate emails. In my method, ``email_me`` is a python script to bypass the ``mail`` command but ``mail`` should work equally well. The ``email_me`` script contains:

.. code-block:: bash

   #!/usr/bin/env python
   import argparse
   import smtplib
   from email.mime.text import MIMEText
   
   parser = argparse.ArgumentParser()
   parser.add_argument("--filename", "-f",type=str, default=None)
   parser.add_argument("--subject", "-s", type=str, default="REMS email")
   parser.add_argument("--address", "-a", type=str, default="DEFAULT_ADDRESS")
   parser.add_argument("--smtp", "-m", type=str, default="DEFAULT_SMTP_SERVER")
   args = parser.parse_args()
   if args.filename is not None:
       fp = open(args.filename,'rb')
       msg = MIMEText(fp.read())
       fp.close()
   else:
       msg = MIMEText("")
   
   
   msg["Subject"] = args.subject
   msg["From"] = args.address
   msg["To"] = args.address
   try:
       s = smtplib.SMTP(args.smtp)
       s.sendmail(args.address,args.address,msg.as_string())
       s.quit()
   except:
       pass

