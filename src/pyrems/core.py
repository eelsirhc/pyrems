"""The core REMS class and methods"""

## Copyright (c) 2012, Christopher Lee, 
## All rights reserved.
import logging
from pyrems.utils import *
from pyrems.rxml import remsXML, writeXML
import os
import glob

loggers = {}

class remsException(Exception):
    """A simple class to deal with exceptions thrown by the code. We define a message
    and an error code for the report, but do nothing further."""
    def __init__(self, message, errorcode):
        Exception.__init__(self, message)
        self.errorcode = errorcode
    
class rems(object):
    def __init__(self,log=False):
        """Initialization function. If log=True then we configure the logging code to output a log
        to a file called rems.log"""
        self.log=log
        if self.log:
            self.SetupLog()
        
    def SetupLog(self,name="pyrems"):
        """Set up log to file for debugging and log to terminal for fatal errors. Debug will generally
        inform you when anything happens (at debug level or higher). Error will inform you when there
        is a problem that will likely result in failure."""
        global loggers

        if loggers.get(name):
            self.logger = loggers[name]
            return 
        
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)
        #file for DEBUG
        fh = logging.FileHandler("rems.log")
        fh.setLevel(logging.DEBUG)
        #console for ERRORS
        ch=logging.StreamHandler()
        ch.setLevel(logging.ERROR)
        #colorize console
        class MyFormatter(logging.Formatter):
            def __init__(self, fmt, datefmt=None):
                logging.Formatter.__init__(self, fmt, datefmt)
            def format(self,record):
                str = logging.Formatter.format(self, record)
                return colorize(str, record.levelname)
        form = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        datefmt = "%Y/%m/%d %H:%M:%S "

        formatter = logging.Formatter(form, datefmt=datefmt)
        fh.setFormatter(formatter)

        lform = logging.Formatter(form,datefmt=datefmt)
        lform = logging.Formatter(form)
        ch.setFormatter(lform)
        
        self.logger.addHandler(fh)
        self.logger.addHandler(ch)
        loggers[name]=self.logger

    def Info(self, message):
        """Print info message to the log (wrapper)"""
        if self.log:
            self.logger.info(message)
        
    def Fatal(self, message):
        """print fatal message and raise exception (wrapper)"""
        if self.log:
            self.logger.error(message)
        raise remsException(message, message)
        
    def Start(self, name="Unknown Program"):
        """info start of function (wrapper to self.info)"""
        if self.log:
            self.Info("Start {0}".format(name))
    
    def End(self, name="Unknown Program"):
        """info end of function (wrapper to self.info)"""
        if self.log:
            self.Info("End {0}".format(name))


    def DownloadFromSource(self, urlname, filename):
        """Download `urlname` and save to `filename` using urllib.
        Typical usage is to download the rems_weather.xml from the CAB website and save it to a
        file called 'latest_rems_weather.xml
        """
        self.Info("Downloading from {urlname} to {filename}".
            format(urlname=urlname, filename=filename))
        
        import urllib
        import os
        
        if os.path.exists(filename):
            self.Fatal("File {filename} exists".format(filename=filename))
        try:
            urllib.urlretrieve(urlname, filename)
        except IOError as e:
            self.Fatal("Error while retrieving data: {0}".format(e))
            
        return
        
    def CompareTwoFiles(self, newfile, oldfile):
        """Compare `newfile` and `oldfile` line by line. returns the sameness value (True=same file).
        This is something like a diff, or an md5sum, but exists as soon as a difference in the text is
        found without regard to the meaning of the difference (whitespace changes in XML files are considered changes)...
        """

        if not os.path.exists(newfile):
            self.Fatal("File {newfile} does not exist".format(newfile=newfile))
        elif not os.path.exists(oldfile):
            return False

        same = True

        #file size
        s1 = os.stat(newfile)
        s2 = os.stat(oldfile)

        if s1.st_size != s2.st_size:
            same = False
        else:
            h1 = open(newfile)
            h2 = open(oldfile)
            same=True
            for l1, l2 in zip(h1, h2):
                if l1!=l2:
                    same=False
                    break
            h1.close()
            h2.close()
        
        return same
        
    def DeleteFile(self, filename):
        """Delete the file `filename`. This is used to remove old rems_weather.xml files"""
        self.Info("Deleting {filename}".format(filename=filename))
        if not os.path.exists(filename):
            self.Fatal("Deleting non-existent file {filename}".format(filename=filename))
        #delete the file
        os.unlink(filename)
        return
        
    def ArchiveNewFilename(self, filename):
        """Create a new filename by appending the date as YYYYMMSS_HHMMSS onto the filename.
        This doesn't create the file, just the filename."""
        import datetime
        suffix = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        new_filename = "{filename}.{suffix}".format(filename=filename, 
                                                    suffix=suffix)
        return new_filename
        
    def ArchiveFile(self, filename, 
                            operational_filename, 
                            operational_directory="./",
                            archive_dir='./archive'):
        """Archive the file `filename` into the `archive_dir` after creating a new
        datestamped filename."""
        self.Info("Archiving {filename}".format(filename=filename))
        #check for the file
        if not os.path.exists(filename):
            self.Fatal("Archiving Non-existent file {filename}".format(filename=filename))

        #make the archive directory if needed
        if not os.path.exists(archive_dir):
            self.Info("Making directory {archive}".format(archive=archive_dir))
            os.makedirs(archive_dir)

        #check the destination file
        full_op_filename = os.path.join(operational_directory,operational_filename)
        if os.path.exists(full_op_filename):
            self.Fatal("Destination file already exists {filename}".format(filename=full_op_filename))
        
        #construct the archive filename
        arch_filename = os.path.join(archive_dir, self.ArchiveNewFilename(operational_filename))
        
        #check the archive filename
        if os.path.exists(arch_filename):
            self.Fatal("Archive file already exists {filename}".format(filename=arch_filename))

        #copy the file to the archive and operational
        import shutil
        shutil.copyfile(filename, arch_filename)
        shutil.copyfile(filename, full_op_filename)
        return arch_filename, full_op_filename
        
        
    def ParseXML(self, filename):
        """Parse the XML using the remsXML object and return the object"""
        if not os.path.exists(filename):
            self.Fatal("XML file does not exist {filename}".format(filename=filename))
        
        #parse
        xdata = remsXML(filename)
        return xdata
        

    def XMLtoJSON(self, filename, output, compression=0):
        """Convert the contents of the XML file to a JSON file, with optional compression of the 
            text fields. See the rxml file for compression options."""
        
        if isinstance(filename, list):
            #for each fname in the filename list
            #glob that fname 
            #for each item in that glob
            #store it.
            filenames = [item for fname in filename for item in glob.glob(fname)]
        else:
            filenames = glob.glob(filename)
        filenames.sort()
        if not len(filenames):
            self.Fatal("XML file does not exist {filename}".format(filename=filename))
        if os.path.exists(output):
            self.Fatal("JSON file already exists {output}".format(output=output))
        #parse
        
        out = open(output,'w')
        
        if len(filenames) > 1:
            out.write("[")
            for i,f in enumerate(filenames):
                xdata = remsXML(f)
                data = xdata.JSON(compression=compression)
                out.write(data)
                if i!=len(filenames)-1:
                    out.write(", ")
            out.write("]")
        else:
            xdata = remsXML(filenames[0])
            data = xdata.JSON(compression=compression)
            out.write(data)
        
        out.close()
        
        return xdata
        
    def XMLtoAshimaClimate(self, filename, output, compression=0,fix=False):
        """Convert the contents of the XML file to an Ashima generated 'mars_climate.xml' file."""
        if isinstance(filename, list):
            filenames = [item for fname in filename for item in glob.glob(fname)]
        else:
            filenames = glob.glob(filename)
        filenames.sort()
        if not len(filenames):
            self.Fatal("XML file does not exist {filename}".format(filename=filename))

        if len(filenames) > 0:
            xml = None
            for i,f in enumerate(filenames):
                xdata = remsXML(f)
                if fix:
                    xdata.addCalculated()
                if i==0:
                   #define the header content
                   xml = xdata.climate_header()

                xml.append(xdata.record())
        writeXML(xml, output)

        return None