from ordereddict import OrderedDict as odict
import sunrise
import marstime
from datetime import datetime
import fixes
import xml.etree.ElementTree as ElementTree

## Copyright (c) 2012, Christopher Lee, 
## All rights reserved.
def convert(val):
    """attempts to convert a string `val` into an integer or float,
    before giving up and returning val."""
    
    try:
        val=int(val)
    except:
        try:
            val=float(val)
        except:
            val = str(val or "\0") #\0 is nothing, but not empty.
    finally:
        return val
    
def writeXML(element, filename):
    """Wraps the xml ElementTree class to write the XML file"""
    ElementTree.ElementTree(element).write(filename, encoding="utf-8")

class remsXML(object):
    """REMS XML interface class"""
    def __init__(self,filename=None):
        """Initialization function. Takes the filename and reads the xml from that file.
            Further defines the keys we are interested in from the XML file."""
        if filename is not None:
            self.read_file(filename)
        
        self.keylist = ["min_temp", "max_temp", "pressure", "abs_humidity", 
                    "wind_speed", "wind_direction", "atmo_opacity", 
                    "season", "ls", "sunrise", "sunset"]

    def read_file(self, filename):
        """Reads the XML file as raw data into self.raw, and a parsed XML file
        into the dictionary self.header and the ordered dictionary self.processed.
        REMS pressure values used to come as hPa, but now come as Pa. 
        The pressure data stored in this class is always hPa and any value above 500 hPa
        is considered to be in units of Pa instead.
        
        """
        #raw
        f = open(filename)
        self.raw = f.read()
        f.close()
        #parse
        self.data = ElementTree.parse(filename)
        self.use_headers=["sol","terrestrial_date"]
        self.header = {}
        self.description = {}
        for k in ["title","link","sol","terrestrial_date"]:
            try:
                self.header[k] = convert(self.data.find(k).text)
            except:
                self.header[k] = ""
        self.ignore = ["min_gts_temp","max_gts_temp", "local_uv_irradiance_index"]
        self.processed = odict([ ("min_temp",odict(unit="K", title="Minimum Temperature")),
                                ("max_temp", odict(unit="K", title="Maximum Temperature")),
                                ("pressure", odict(unit="Pa", title="Pressure")),
                                ("pressure_string", odict(unit="", title="Pressure String")),
                                ("abs_humidity", odict(unit="%", title="Absolute Humidity")),
                                ("wind_speed", odict(unit="m/s", title="Wind Speed")),
                                ("wind_direction", odict(unit="", title="Wind Direction")),
                                ("atmo_opacity", odict(unit="", title="Atmospheric Opacity")),
                                ("season", odict(unit="", title="Season")),
                                ("ls", odict(unit="deg", title="Ls")),
                                ("sunrise", odict(unit="", title="Sunrise Time")),
                                ("sunset", odict(unit="", title="Sunset Time"))])
        magnitudes = self.data.find("magnitudes")
        for v in magnitudes.findall("*"):
            k=v.tag
            if k.endswith("_desc") or k.endswith("_des") or self.ignore.count(k):
                #descriptions
                self.description[k] = v.text
            elif k in self.processed:
                self.processed[k]["value"] = convert(v.text)
            else:
                self.processed[k] = odict(unit="",title="", 
                            value=convert(v.text))
            if k=="pressure" and\
                (isinstance(self.processed[k]["value"],float) or isinstance(self.processed[k]["value"],int))\
                and self.processed[k]["value"] > 500.:
                self.processed[k]["value"]=self.processed[k]["value"]/100.
                
    
    def String(self):
        """Pretty prints the ordered dictionary representation of the XML file as ascii"""
        strhead = "{title} \n({link}) \nsol {sol}\n\n".format(**self.header)
        strdata = ""
        for k in self.keylist:
            strdata = strdata + "{title} = {value} {unit}\n".format(**self.processed[k])
        return strhead+strdata
        
    def __str__(self):
        """Returns the raw file contents."""
        return self.raw
    
    def compress(self, compression=0):
        """Performs some simple compression the ascii fields:
        0: no compression
        1: shorten the internal field names
        2: 1 and covert the data to a list of [title,value,units]
        3: 0 but only returning the value (closest to the XML)
        4: 0+1
        9: convert the odictionary to a list of values only, with no header data or labels"""
        mapping = odict( min_temp="lt",
                        max_temp="ht",
                        pressure="p",
                        abs_humidity="h", 
                        wind_speed="ws",
                        wind_direction="wd",
                        atmo_opacity="ao",
                        season="s",
                        ls="ls",
                        sunrise="su",
                        sunset="sd")
        if compression == 9:
            #LISTIFY!
            res=[self.header["sol"]]
            res.extend(map(lambda x: self.processed[x]["value"], self.processed.keys()))
        else:
            res={}
            res["sol"]=self.header["sol"]
            res["terrestrial_date"]=self.header["terrestrial_date"]

            for k in self.processed.keys():
                if k in mapping:
                    v = mapping[k]
                else:
                    v=k
                data = self.processed[k]
                if compression==0:
                    res[k]=data
                if compression==1:
                    res[v]=data
                elif compression==2:
                    res[v] = [data["title"],data["value"],data["unit"]]
                elif compression==3:
                    res[k] = data["value"]
                elif compression==4:
                    res[v]=data["value"]

        return res
        
    def record(self):
        """Generate a new record entry from the XML file. Each record entry is wrapper
        in <record> and contains the entries in the <magnitude> entry of rems_weather.xml
        and the header components sol and terrestrial date."""
        ET=ElementTree
        record = ET.Element("record")
        for h in self.use_headers:
            r = ET.SubElement(record, h)
            r.text = str(self.header[h])
        magnitudes = ET.SubElement(record, "magnitudes")
        for k,v in self.processed.items():
            r = ET.SubElement(magnitudes,k)
            r.text = str(v["value"])
        return record

    def addCalculated(self):
        """Tries to fix some stupidity in the data"""
        #get the sol
        sol = self.header["sol"]
        #midnight zero is the number of SI milliseconds since 1970/1/1 
        midnight_zero = 1344174593000. #1344231060000 - 56466000; //midnight before landing.
        
        #to get j2k start with sol, multiply by 86400*1.02..*1e3 and add to midnight zero
        #then remove j2k milliseconds from that number
        from_1970 = 86400e3 * 1.027491252 * sol + midnight_zero
        j2k = from_1970 - 946728000e3
        j2k = j2k/86400e3
        longitude, latitude = -137.4,-4.5
        
        sup, sdown = sunrise.sunrise_sunset(j2k , longitude, latitude, solar_angular_radius=0.0)
        Mxup = marstime.Local_Mean_Solar_Time( longitude, sup) #sunrise
        Mxdown = marstime.Local_Mean_Solar_Time( longitude, sdown) #sunset
        Txup = marstime.Local_True_Solar_Time( longitude, sup) #sunrise
        Txdown = marstime.Local_True_Solar_Time( longitude, sdown) #sunset
        self.processed["cmu"] = odict(unit="", title="Calculated Mean Sunrise Time", 
                                            value=sunrise.str_hm(Mxup))
        self.processed["cmd"]  = odict(unit="", title="Calculated Mean Sunset Time", 
                                            value=sunrise.str_hm(Mxdown))
        self.processed["ctu"] = odict(unit="", title="Calculated True Sunrise Time", 
                                            value=sunrise.str_hm(Txup))
        self.processed["ctd"]  = odict(unit="", title="Calculated True Sunset Time", 
                                            value=sunrise.str_hm(Txdown))


        self.processed["cls"]= odict(unit="", title="Calculated Ls", 
                                            value= "{0:4.1f}".format(marstime.Mars_Ls(j2k)))
        return
        
#    var cur_lon = east_to_west(137.440247); //222.5781  ; //5981;  // + 42/60. ; //0.0;
        return
        
    def climate_header(self):
        """Defines the XML element header <climate_report> and returns the XML object"""
        ET=ElementTree
        record = ET.Element("climate_report")
        for h in ["title","link"]:
            r = ET.SubElement(record, h)
            r.text = str(self.header[h])
        return record
        
    def JSON(self, compression=0):
        """Create a odictionary or list of data, and convert into a JSON format"""
        import json
        
        data = self.compress(compression)
        return json.dumps(data)
    
    def fixDates(self):
        d = self.header["terrestrial_date"]
        dd = fixes.fixDateWithFormat(d)
        self.processed["isod"] = odict(unit="",title="", value=dd.strftime("%Y-%m-%d"))
        self.use_headers.append("title")
        self.use_headers.append("link")

    def nullWinds(self):
        self.processed["wind_direction"]["value"] = "--"
        self.processed["wind_speed"]["value"] = "--"
        self.processed["pressure_string"]["value"] = "--"
        