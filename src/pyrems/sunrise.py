#!/usr/bin/env python

## Copyright (c) 2012, Christopher Lee, 
## All rights reserved.

import marstime
import datetime
import math
import argparse

def midnight(date, longitude, latitude):
    """Given a Mars Solar Date(MSD) and location, find the local midnight times.
    The input data is used to calculate the local True solar time (LTST)
    and the local midnight occurs LTST hours before and 24-LTST after"""
    lt = marstime.Local_True_Solar_Time(longitude, date)
    mid1 = date - lt/24.
    mid2 = date + (24-lt)/24.
    return (mid1, mid2)

def solelev(date, x,y, solar_angular_radius=0.0):
    """a wrapper for scipy.optimize to reverse the arguments for solar_elevation"""
    return marstime.solar_elevation(x,y,date)+solar_angular_radius
    
def bisect(f, a,b, args=None,xtol=1e-12,maxit=100):
    if maxit < 0:
        raise ValueError("maxit < 0 in bisect")
        
    it=0
    l=a
    h=b
    while it < maxit:
        m = (l+h)/2.
        if args:
            fl = f(l,*args)
            fh = f(h,*args)
            fm = f(m,*args)
        else:
            fl,fh,fm = f(l), f(h), f(m)
        if abs(fm) < xtol:
            break
        if fl * fh > 0:
            raise ValueError("bisect error. low and high values have the same sign, lost root.")
        if fl * fm > 0:
            l=m
        else:
            h=m
        it = it +1
        if it > maxit:
            print("Exceeded maximum iteration in bisect, maxit={0}".format(maxit))
            break
        
        #same sign
    return m
                
def sunrise_sunset(date, longitude, latitude, solar_angular_radius=0.0):
    """Interface to the scipy.optimize.
    Using the date (j2000 offset) and location, start by finding the local 
    midnights. the local mid-day is then (roughly) at the center of the two 
    midnights. Sunrise must occur between midnight and midday, sunset between 
    midday and midnight (except for polar night).
    
    This method uses Ian's method, which is less annoying than my method that required
    a conditional depending on whether 'date' was in the daytime or nighttime."""
    
    mid1,mid2=midnight(date, longitude, latitude)
    noon = 0.5*(mid1+mid2)
    sunrise = bisect(solelev, mid1, noon, args=(longitude, latitude, solar_angular_radius))
    sunset = bisect(solelev, noon, mid2, args=(longitude, latitude, solar_angular_radius))
    return sunrise, sunset
    
def hms(h):
    """Converts float hours to integer hours, minutes, seconds. loses milliseconds"""
    hi = int(h)
    m = 60.*(h-hi)
    mi = int(m)
    s = 60.*(m-mi)
    si = int(s)
    return hi,mi,si
    
def str_hms(h, prefix="", suffix=""):
    """String formatted time prefix HH:MM:SS suffix"""
    h,m,s = hms(h)
    return("{0} {1:02d}:{2:02d}:{3:02d} {4}".format(prefix, h,m,s, suffix).strip())
    
def str_hm(h, prefix="", suffix=""):
    """String formatted time prefix HH:MM suffix"""
    h,m,s = hms(h)
    return("{0} {1:02d}:{2:02d} {3}".format(prefix, h,m, suffix).strip())


def sun_angular_radius(jdate, solar_radius = 6.96342e8, one_au=1.496e11):
    """Calculate the angular size of the sun at the specified date. Assumes that
    solar radius is 6.9e8m, one AU is 1.496e11m"""
    #as Google usefully says, solar_radius = 1 solar radius
    #wikipedia solar radius 6.96342x10^5 km
    # 1 Au 1.496x10^8 km
    orbit_radius = marstime.heliocentric_distance(jdate) #in AU.
    angular_radius = solar_radius / (orbit_radius * one_au)
    angular_radius_degree = math.degrees(angular_radius)
    return angular_radius_degree
    

def simple_julian_offset(indate):
    """Simple conversion from date to J2000 offset"""
    datetime_epoch = datetime.datetime(2000,1,1,12,0,0)
    date = indate-datetime_epoch
    jdate = date.days + date.seconds/86400.
    return jdate
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plots and calculates sunrise/sunset times for a spherical flat Mars")
    parser.add_argument("date",type=str, nargs="?",
            help="Earth date in ISO format YYYY/MM/DD or MSD in DDDDD")
    parser.add_argument("--msd","-m", action='store_true', 
            help="Use Mars Solar Date instead of Earth Date")
    parser.add_argument("--ignore_solar_radius", action='store_true',
            help="calculate the time for the solar center, not the 'top' edge.")
    parser.add_argument("--longitude","-x",default=137.44,type=float,
            help="East Longitude")
    parser.add_argument("--latitude","-y",default=-4.5, type=float,
            help="North Latitude")
    parser.add_argument("--scale_marker","-s",default=1.0, type=float,
            help="Scale marker size")
    parser.add_argument("--use_marker_radius",action="store_true",
            help="Use the marker radius defined in marker_radius, instead of scaling properly")
    parser.add_argument("--marker_radius",type=float, default=2.0,
            help="set the marker radius in degrees, requires --use_marker_radius")
    parser.add_argument("--number_of_points","-n",type=int, default=72,
            help="set the number of markers to plot, evenly spaced throughout the day")
    parser.add_argument("--filename",type=str, default="sunrise.png",
            help="set the output filename and type")
    args = parser.parse_args()
    
    
    if args.date is None: #nodate, use now
        default_date = datetime.datetime.now()
        jdate = simple_julian_offset(default_date)
        output_date = default_date.strftime("%Y/%m/%d")
    elif args.msd: #Mars solar date
        output_date = args.date
        jdate = marstime.j2000_from_Mars_Solar_Date(args.date)
    else: #earth date
        output_date = args.date
        jdate = simple_julian_offset(datetime.datetime.strptime(args.date, "%Y/%m/%d"))
     
    #convert to west longitude
    west_longitude = marstime.east_to_west(args.longitude)
    north_latitude = args.latitude
    #find the midnight times
    mdate = midnight(jdate, west_longitude, north_latitude)

    #calculate the angular radius of the Sun to offset the bissect calculation
    solar_angular_radius = 0.0
    if not args.ignore_solar_radius:
        solar_angular_radius = sun_angular_radius(jdate)
    
    sup, sdown = sunrise_sunset(jdate, west_longitude, north_latitude, solar_angular_radius = solar_angular_radius)
    xup = marstime.Local_Mean_Solar_Time( west_longitude, sup) #sunrise
    xdown = marstime.Local_Mean_Solar_Time( west_longitude, sdown) #sunset
    
    print str_hms(xup, prefix="Sunrise at LMST of ")
    print str_hms(xdown, prefix="Sunset at LMST of ")
    print str_hms(marstime.Local_True_Solar_Time( west_longitude, sup), prefix="Sunrise at LTST of ")
    print str_hms(marstime.Local_True_Solar_Time( west_longitude, sdown), prefix="Sunset at LTST of ")

    
    #make a plot
    plot_solar_elevation(args, mdate, xup, xdown, output_date)
