"""
Contains the executable commands provided by the pyREMS package. Each command is a sub-command
of the executable `pyrems`
"""

from pyrems.core import *
from argh import ArghParser, arg, command
import sys

@command
def downloadXML(urlname="http://cab.inta-csic.es/rems/rems_weather.xml", 
#urlname="https://www.ashimaresearch.com/~lee/rems_weather.xml",#"http://cab.inta-csic.es/rems/rems_weather.xml", 
             dirname="./", 
             filename="latest_rems_weather.xml",
             nolog=False):
    """Download the file from `urlname` and save to `dirname/filename`, 
        disable logging with --nolog
        
        The command can be run as 'pyrems downloadXML' to use the default values
        and download the rems_weather.xml from the CAB and store it as 'latests_rems_weather.xml'
        
        """

    ob = rems(log=not nolog)
    ob.Start("Download")
    
    try:
        filename = os.path.join(dirname, filename)
        if os.path.exists(filename):
            if not os.path.getsize(filename):
                ob.Info("{0} exists and is empty, removing".format(filename))
                ob.DeleteFile(filename)
        ob.DownloadFromSource(urlname, filename)
    except remsException as e:
        raise
    finally:
        ob.End("Download")

@command
def compareXML(newfilename="latest_rems_weather.xml", oldfilename="rems_weather.xml",
            archive="./archive", opdir="./",
            nolog=False):
    """Compare `newfilename` with `oldfilename`.
    If they are the same, delete `newfilename`.
    If they are different, delete `oldfilename`, archive `newfilename` with a datestamp,
    and copy `newfilename` to `oldfilename`.
    
    Running as `pyrems compareXML` compares the file 'new' file latest_rems_weather.xml
    with the old file 'rems_weather.xml'."""
    
    ob = rems(log=not nolog)
    ob.Start("Compare Files")
    try:
        full_op_filename = os.path.join(opdir,oldfilename)
        same = ob.CompareTwoFiles(newfilename,full_op_filename)
        if not same:
            ob.Info("New file downloaded")
            if os.path.exists(full_op_filename):
                ob.DeleteFile(full_op_filename)
            
            arch_filename, full_op_filename = ob.ArchiveFile(newfilename, oldfilename, opdir, archive)
            ob.DeleteFile(newfilename)
            return same, arch_filename, full_op_filename
        else:
            #same file, delete the new file
            ob.Info("Same file downloaded")
            ob.DeleteFile(newfilename)
        return same, None, None
    except remsException as e:
        raise
    finally:
        ob.End("Compare Files")
    
@command
def printXML(filename="rems_weather.xml",nolog=False):
    """Print the contents of the XML file `filename`. 
    
    Running as `pyrems printXML` prints out the contents of the `rems_weather.xml` file"""
    ob = rems(log=not nolog)
    ob.Start("Print XML")
    
    try:
        xdata = ob.ParseXML(filename)
        print xdata
    except remsException as e:
        raise e
    finally:
        ob.End("Print XML")

@command
def fixXML(filename="rems_weather.xml",output=None, nolog=False):
    """Print the contents of the XML file `filename`. 
    
    Running as `pyrems printXML` prints out the contents of the `rems_weather.xml` file"""
    ob = rems(log=not nolog)
    ob.Start("Fix XML")
    
    try:
        xdata = ob.ParseXML(filename)
        xdata.fixDates()
        xdata.addCalculated()
        xdata.nullWinds()
        if output is None:
            output = filename
        writeXML(xdata.record(),output)
    
    except remsException as e:
        raise e
    finally:
        ob.End("Fix XML")



@command
def printJSON(filename="rems_weather.xml",compression=0, nolog=False):
    """Convert the XML file `filename` into JSON after optional field `compression`.
    
    Running as `pyrems printJSON` converts the `rems_weather.xml` to JSON verbosely."""
    ob = rems(log=not nolog)
    ob.Start("Print JSON")
    
    try:
        xdata = ob.ParseXML(filename)
        print xdata.JSON(compression=compression)
    except remsException as e:
        raise e
    finally:
        ob.End("Print JSON")

@command
def printString(filename="rems_weather.xml", nolog=False):
    """Pretty Print the XML file `filename` as plain text.
    
    Running as `pyrems printString` prints the contents of rems_weather.xml as a string"""
    ob = rems(log=not nolog)
    ob.Start("Print String")
    
    try:
        xdata = ob.ParseXML(filename)
        print xdata.String()
    except remsException as e:
        raise e
    finally:
        ob.End("Print String")

@arg("--filename", "-f",type=str, nargs="+", default=["archive/rems_weather.xml*"])
@arg("--output", "-o", type=str, default="rems_climate.xml")
@arg("--delete","-d", action='store_false')
@arg("--nolog", action='store_true')
@arg("--compression", default=0)

def writeJSON(args): #filename="rems_weather.xml",output="rems_weather.json",compression=0,delete=True,nolog=False):
    """Convert the XML file `filename` into JSON after optional field `compression`, saving as `output`.
    
    Running as `pyrems writeJSON` converts the rems_weather.xml file to JSON stored in rems_weather.json, with
    no compression and will delete any old rems_weather.json file first."""
    if isinstance(args, dict):
        filename, output, delete, nolog, compression = args["filename"],\
                                                    args["output"],\
                                                    args["delete"],\
                                                    args["nolog"],\
                                                    args["compression"]
    else:
        filename, output, delete, nolog, compression = args.filename,\
                                                    args.output,\
                                                    args.delete,\
                                                    args.nolog,\
                                                    args.compression

    ob = rems(log=not nolog)
    ob.Start("Save JSON")
    
    try:
        if delete:
            if os.path.exists(output):
                ob.Info("Deleting file {output}".format(output=output))
                os.unlink(output)
            
        xdata = ob.XMLtoJSON(filename, output, compression=compression)

    except remsException as e:
        raise e
    finally:
        ob.End("save JSON")

@arg("--filename", "-f",type=str, nargs="+", default=["archive/rems_weather.xml*"])
@arg("--output", "-o", type=str, default="rems_climate.xml")
@arg("--delete","-d", action='store_false')
@arg("--nolog", action='store_true')
@arg("--fix", action='store_true')
def writeClimate(args): #filename=["archive/rems_weather.xml*"],output="rems_climate.xml",delete=True,nolog=False):
    """Convert the XML file `filename` into a rems_climate like file.
    
    Running as `pyrems writeClimate` will convert all rems_weather.xml* files in the archive directory
    to a rems_climate.xml file in the current directory, deleting the old rems_climate.xml file.
    """
    if not isinstance(args, dict):
        args = vars(args)
    #filename=["archive/rems_weather.xml*"],output="rems_climate.xml",delete=True,nolog=False):
    ob = rems(log=not args["nolog"])
    ob.Start("Save Climate")
    
    try:
        if args["delete"]:
            if os.path.exists(args["output"]):
                ob.Info("Deleting file {output}".format(output=args["output"]))
                os.unlink(args["output"])
            
        xdata = ob.XMLtoAshimaClimate(args["filename"], args["output"], fix=args["fix"])

    except remsException as e:
        raise e
    finally:
        ob.End("save Climate")

@command
def archiveLog(logfile="rems.log",nolog=False,threshold=1024*1024):
    """Archives the log file if it gets too large.
        Running as `pyrems archiveLog` checks if the file size is over 1MB, and moves the file to a files
        called rems.log.YYYYMMDD_HHMMSS if it is."""
    ob = rems(log=not nolog)
    if os.path.getsize(logfile) > threshold:
        ob.Start("Moving Log file")
        import datetime
        suffix = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        import shutil
        arch = "{logfile}.{suffix}".format(logfile=logfile,suffix=suffix)
        shutil.move(logfile, arch)
        ob.End("Moved Log File")


@command
def process(urlname="http://cab.inta-csic.es/rems/rems_weather.xml", 
            dirname="./", 
            filename="latest_rems_weather.xml",
            oldfilename="rems_weather.xml",
            archive="./archive",
            modified="./modified",
            output="rems_weather.json",
            climate_output="rems_climate.xml",
            modified_climate_output="c_rems_climate.xml",
            compression=9,
            nolog=False, success_callback=None,fix=False):
    """wrapper around all subfunctions: 
        This command downloads the `urlname` to `filename` and compares it with `oldfilename`.
        If the data is new, `oldfilename` is deleted and `filename` is archived, then the entire archive
        is used to create the JSON file history.rems_weather.json and the XML file `climate_output`.
        
        If requested, a script `success_callback` is called if the last download contained new data. e.g.
        This script can email the XML file, or copy it to the server,etc. In my example script I run 
        `tidy --quiet yes --wrap 0 -xml -m -i rems_climate.xml` to clean up the XML to make 
        it human readable again, before emailing the contents.
    """
        
    ob = rems(log=not nolog)
    ob.Start("Processing")
    try:
        downloadXML(urlname=urlname, dirname=dirname, filename=filename, nolog=nolog)
        same, arch_name, op_name = compareXML(newfilename=filename, oldfilename=oldfilename, archive=archive, opdir=dirname,nolog=nolog)
        if not same or not os.path.exists(output):
            writeJSON(dict(filename=oldfilename, output=output, compression=compression,delete=True,nolog=nolog))
            writeJSON(dict(filename="{0}/{1}.*".format(archive,oldfilename), output=climate_output.replace("xml","json"),  compression=compression,delete=True,nolog=nolog))
            writeClimate(dict(filename="{0}/{1}.*".format(archive,oldfilename), output=climate_output,delete=True,nolog=nolog,fix=False))
            if fix:
                if not os.path.exists(modified):
                    os.makedirs(modified)
                if arch_name is not None:
                    fixXML(filename=arch_name,output=arch_name.replace(archive,modified))
                writeClimate(dict(filename="{0}/{1}.*".format(modified,oldfilename), 
                            output=modified_climate_output,delete=True,nolog=nolog,fix=False))
                writeJSON(dict(filename="{0}/{1}.*".format(modified,oldfilename), 
                        output=modified_climate_output.replace("xml","json"), compression=compression,delete=True,nolog=nolog))
                
            
            
            if success_callback is not None:
                ob.Info("Success callback: {0}".format(success_callback))
                import subprocess
                subprocess.call(success_callback.split())
        archiveLog(nolog=nolog)
    except remsException as e:
        raise
    finally:
        ob.End("Processing")

def main():
    """wrapper around the commands"""
    parser = ArghParser()
    parser.add_commands([downloadXML, #downloads
                        compareXML,   #compares
                        printXML,     #prints XML
                        printJSON,    #prints JSON
                        writeJSON,    #saves JSON
                        fixXML, #fix XML
                        printString,  #prints ascii
                        process,      #downloads, compares, saves
                        archiveLog,   #rotate logs
                        writeClimate])#merges the archive directory into one file
    try:
        parser.dispatch()
        sys.exit(0) 
    except remsException as e:
        sys.exit(e.errorcode)

