from datetime import datetime
from ordereddict import OrderedDict as odict

def cleanDateString(string):
    months=odict([  ("ene","Jan"),
                    ("Ene","Jan"),
                    ("Sept.","Sep"),
                    ("Sept","Sep"),

                    ("UTC","")
                    ])
    for k,v in months.items():
        if string.count(k):
            string=string.replace(k,v)
    return string.strip()
    
def fixDateWithFormat(string, format=["%b %d, %Y","%d %b, %Y","%B %d, %Y","%d %B, %Y"]):
    """Tries to parse the date string into an datetime object"""
    parsed=False
    string = cleanDateString(string.strip())
    if isinstance(format,list):
        for f in format:
            try:
                if not parsed:
                    dd = datetime.strptime(string,f)
                    parsed=True
            except ValueError as e:
                pass
    else:
        return self.fixDateWithFormat(string,[format])
    
    if parsed:
        return dd
    else:
        return None
