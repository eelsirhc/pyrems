from setuptools import setup, find_packages
import sys, os

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.rst')).read()
NEWS = open(os.path.join(here, 'NEWS.txt')).read()


version = '0.22'

install_requires = [
    "logging", 
    "argh", 
    "ordereddict",
    "marstime",
    # List your project dependencies here.
    # For more details, see:
    # http://packages.python.org/distribute/setuptools.html#declaring-dependencies
]


setup(name='pyrems',
    version=version,
    description="0.22",
    long_description=README + '\n\n' + NEWS,
    classifiers=[
      # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    ],
    keywords='',
    author='Christopher Lee',
    author_email='lee@ashimaresearch.com',
    url='https://bitbucket.org/eelsirhc/pyrems',
    license='copyright Ashima Research',
    packages=find_packages('src'),
    package_dir = {'': 'src'},include_package_data=True,
    zip_safe=False,
    install_requires=install_requires,
    entry_points={
        'console_scripts':
            ['pyrems=pyrems:main']
    }
)
